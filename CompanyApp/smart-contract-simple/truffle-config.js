var HDWalletProvider = require("truffle-hdwallet-provider");
const MNEMONIC = '8E92643EC295A004104D97C416568A253C10430EEA5EE795DD836FA8E366566B';

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    ropsten: {
      provider: function() {
        return new HDWalletProvider(MNEMONIC, "https://ropsten.infura.io/v3/82ab725d0f624783b1b5a70bdc7d4fbb")
      },
      network_id: 3,
      gas: 4000000      //make sure this gas allocation isn't over 4M, which is the max
    }
  }
};