let selectedPartNumber = null;
let selectedContractAddressForTemperatures = null;
let contractAddress = null;
let ethereum_network;
let connection = null;

$(function () {
    "use strict";

    // for better performance - to avoid searching in DOM
    var content = $('#content');
    var input = $('#input');
    var status = $('#status');
    let charts = $('.sensorData').collapse('hide');

    $('#progressBox').collapse('hide');

    // my color assigned by the server
    var myColor = false;
    // my name sent to the server
    var myName = false;

    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // if browser doesn't support WebSocket, just show some notification and exit
    if (!window.WebSocket) {
        content.html($('<p>', {
            text: 'Sorry, but your browser doesn\'t '
                + 'support WebSockets.'
        }));
        input.hide();
        $('span').hide();
        return;
    }

    // open connection
    connection = new WebSocket('ws://127.0.0.1:1337');

    connection.onopen = function () {
        $('#fireProductionButton').removeAttr('disabled').text('Print now!');
    };

    connection.onerror = function (error) {
        // just in there were some problems with conenction...
        content.html($('<p>', {
            text: 'Sorry, but there\'s some problem with your '
                + 'connection or the server is down.'
        }));
    };



    // most important part - incoming messages
    connection.onmessage = function (message) {
        console.log("data received!");
        var json = JSON.parse(message.data);
        console.log(json);
        console.log("message type is: " + json.type);

        // Server sent the contract address, so show it on the GUI
        if (json.type === "newState") {
            console.log("A 'new state' message is received");
            handleNewState(json.state);
        } else

        // Server sends parameters from MondoGB, so update the GUI to show themù
        if (json.type === "parametersFromDatabase") {
            //handleParametersFromDatabase(json.parameters);

            ethereum_network = json.ethereum_network;
            console.log("Network: " + ethereum_network);

            $('#badgePartNumber').text(json.parameters.length);
            for (var index in json.parameters) {
                var param = json.parameters[index];
                console.log(param.partNumber);
                $('#parametersListGroup').append('<li id="partNumber' + param.partNumber + '" class="list-group-item list-group-item-action" onclick="handleClick(id)">' + param.partNumber + ' (' + param.gcodeName + ')</li>')
            }
        } else if (json.type === "historyFromDatabase") {
            //handleParametersFromDatabase(json.parameters);

            $('#historyListGroup').text('');
            ethereum_network = json.ethereum_network;

            console.log(json.entries);
            $('#badgeHistory').text(json.entries.length);
            json.entries.forEach(entry => {
                console.log(entry.contractAddress);
                $('#historyListGroup').append('<li class="list-group-item list-group-item-action" id="historyEntry'+entry.contractAddress+'"  onclick="handlePrintLog(id)" ><b>Replaced Part:</b> '+entry.printLogJson.gcodeName+'<br/><b>Print Log Hash:</b> '+entry.logHash+'<br/><b>Contract Address:</b> ' + `<a target="_blank" href="https://${ethereum_network}.etherscan.io/address/${entry.contractAddress}">${entry.contractAddress}</a>` +'<br/><b>PartNumber:</b> '+entry.printLogJson.partNumber+'<br/><b>Production Date:</b> '+entry.printLogJson.dateDoneEvent+'</li>');
            })

            /*
            for (var historyIndex in json.entries) {
                let entry = json.entries[0].contractAddress;
                console.log(entry);
                $('.historyTable').append(
                    $('<tr>').append(
                        $('td').append(
                            $('a').attr('data-index', entry)
                        )
                    )
                );
            }
            */


        } else if (json.type === "eventFromSmartContract") {
            console.log("handling event: ");
            console.log(json);
            handleNewEvent(json.eventData);
        } else if (json.type === "temperaturesLog") {
            console.log("handling event: ");
            console.log(json);
            renderCharts(charts, json.data.temperatures)
        } else {
            console.error("Message type not recognized.");
        }
    };

    $('#fireProductionButton').click(function () {
        if (selectedPartNumber !== null) {
            var msg = {
                type: 'printRequest',
                partNumber: selectedPartNumber
            };
            connection.send(JSON.stringify(msg));
            //$('#fireProductionButton').prop( "disabled", true ).text("In production...");
            setButtonStyle(true, "In production...");
            $('#progressBox').collapse('show');
        } else {
            alert("Please, select a PartNumber.");
        }
    });

    /**
     * This method is optional. If the server wasn't able to respond to the
     * in 3 seconds then show some error message to notify the user that
     * something is wrong.
     */
    setInterval(function () {
        if (connection.readyState !== 1) {
            status.text('Error');
            input.attr('disabled', 'disabled').val('Unable to comminucate '
                + 'with the WebSocket server.');
        }
    }, 3000);



    function handleNewState(state) {

        console.log("Handling new state: ");
        console.log(state);
        switch (state.type) {
            case "0":
                handleDeployed(state);
                break;
            case "1":
                handleOrderSent(state);
                break;
            case "2":
                handleOrderReceived(state);
                break;
            case "3":
                handlePrintStarted(state);
                break;
            case "4":
                handlePrintDone(state, charts);
                break;
            default:
                console.error("State not recognized!");
                break;
        }
    }


    let blockchainState = $('.blockchainState');
    let state1 = $('#blockchainVar1');
    let state2 = $('#blockchainVar2');
    let state3 = $('#blockchainVar3');
    let state4 = $('#blockchainVar4');
    let state5 = $('#blockchainVar5');
    let state6 = $('#blockchainVar6');
    let state7 = $('#blockchainVar7');
    let state8 = $('#blockchainVar8');

    function handleNewEvent(eventData) {

        console.log("Handling blockchain event.");
        console.log(eventData);

        blockchainState.collapse('show');

        let a = eventData.b_printRequested;
        let b = eventData.b_printOrderReceived;
        let c = eventData.b_printStarted;
        let d = eventData.b_printFinished;

        if(a === true && b === null && c === null && d === null) {
            handleStep1(eventData);
        }
        if(a === true && b === true && c === null && d === null) {
            handleStep2(eventData);
        }
        if(a === true && b === true && c === true && d === null) {
            handleStep3(eventData);
        }
        if(a === true && b === true && c === true && d === true) {
            handleStep4(eventData);
        }


    }

    function handleStep1(eventData) {
        state1.html(`<td>
                                Print requested
                            </td>
                            <td>
                                Smart contract certifies that production request has been sent to the Manufacturer.
                            </td>
                            <td>
                                ${capitalize(eventData.b_printRequested)}.
                            </td>`).removeClass('table').addClass('table-info');
        state5.html(`<td>
                                GCODE Hash Company
                            </td>
                            <td>
                                Smart contract certifies that the hash of the GCODE sent by the Company has been stored in blockchain.
                            </td>
                            <td>
                                ${eventData.gcodeHashCompany}
                            </td>`).removeClass('table').addClass('table-info');
    }

    function handleStep2(eventData) {
        state2.html(`<td>
                                Print order received
                            </td>
                            <td>
                                Smart contract certifies that production request has been received by the Manufacturer.
                            </td>
                            <td>
                                ${capitalize(eventData.b_printOrderReceived)}.
                            </td>`).removeClass('table').addClass('table-info');
        state6.html(`<td>
                                GCODE Hash Manufacturer
                            </td>
                            <td>
                                Smart contract certifies that the hash of the GCODE received by the Manufacturer has been stored in blockchain.
                            </td>
                            <td>
                                ${eventData.gcodeHashReceived}
                            </td>`).removeClass('table').addClass('table-info');
    }

    function handleStep3(eventData) {
        state3.html(`<td>
                                Part in production
                            </td>
                            <td>
                                Smart contract certifies that production of the part has been started.
                            </td>
                            <td>
                                ${capitalize(eventData.b_printStarted)}.
                            </td>`).removeClass('table').addClass('table-info');
        state7.html(` <td>
                                GCODE Hash Printer
                            </td>
                            <td>
                                Smart contract certifies that the hash of the GCODE effectively used in the printer has been stored in blockchain.
                            </td>
                            <td>
                                ${eventData.gcodeHashPrinter}
                            </td>`).removeClass('table').addClass('table-info');
    }

    function handleStep4(eventData) {
        state4.html(`<td>
                                Part produced
                            </td>
                            <td>
                                Smart contract certifies that production of the part has been done.
                            </td>
                            <td>
                                ${capitalize(eventData.b_printFinished)}.
                            </td>`).removeClass('table').addClass('table-info');
        state8.html(`<td>
                                GCODE Hash Print Log
                            </td>
                            <td>
                                Smart contract certifies that the hash of the GCODE of the print log file has been stored in blockchain.
                            </td>
                            <td>
                                ${eventData.printLogHash}
                            </td>`).removeClass('table').addClass('table-info');
    }


    /*
    function connect() {
        connection = new WebSocket('ws://127.0.0.1:1337');

        connection.onclose = function(e) {
            console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
            setTimeout(function() {
                connect();
            }, 1000);
        };

        connection.onerror = function(err) {
            console.error('Socket encountered error: ', err.message, 'Closing socket');
            connection.close();
        };
    }
    */
});

function handleClick(id) {
    selectedPartNumber = id.replace('partNumber', '');
    console.log("Selected new Part Number: " + selectedPartNumber);
    $('#selectedPartnumber').text(' ' + selectedPartNumber);
}
function handlePrintLog(IDcontractAddress) {
    selectedContractAddressForTemperatures = IDcontractAddress.replace('historyEntry', '');
    console.log("Selected smart contract to fetch temperatures: " + selectedContractAddressForTemperatures);
    if (selectedContractAddressForTemperatures !== null) {
        var msg = {
            type: 'temepratureLogRequest',
            contractAddress: selectedContractAddressForTemperatures
        };
        connection.send(JSON.stringify(msg));
    } else {
        alert("Please, select a PartNumber.");
    }
}

function hashShortener(_hash) {
    return _hash.substr(0, 4) + '...' + _hash.substr(_hash.length - 3);
}

function setProgress(value) {
    $("#progressBar")
        .css("width", value + "%")
        .attr("aria-valuenow", value)
        .text(value + "%");
}

function handleDeployed(state) {
    contractAddress = state.contractAddress;

    let shortenedHash = hashShortener(contractAddress);

    setProgress(20);

    $('#stateZeroTable').html(`
                            <td>
                                D
                            </td>
                            <td>
                                Deploy Smart Contract
                            </td>
                            <td>
                               <a target="_blank" href="https://${ethereum_network}.etherscan.io/address/${contractAddress}">Show it on ${ethereum_network} (${shortenedHash}).</a>
                            </td>
                            <td>
                                Deployed.
                            </td>`
    ).removeClass('table').addClass('table-success');
}

function handleOrderSent(state) {
    let transactionHash = state.transactionHash;
    console.log(transactionHash);

    let shortenedHash = hashShortener(transactionHash);

    setProgress(40);

    $('#stateOneTable').html(`
                            <td>
                                1
                            </td>
                            <td>
                                Order Sent
                            </td>
                            <td>
                                <a target="_blank" href="https://${ethereum_network}.etherscan.io/tx/${transactionHash}">Show it on ${ethereum_network} (${shortenedHash}).</a>
                            </td>
                            <td>
                                The order has been sent to the Manufacturer.
                            </td>`
    ).removeClass('table').addClass('table-success');
}

function handleOrderReceived(state) {
    let transactionHash = state.transactionHash;
    console.log(transactionHash);

    let shortenedHash = hashShortener(transactionHash);

    setProgress(60);

    $('#stateTwoTable').html(`
                            <td>
                                2
                            </td>
                            <td>
                                Order Received
                            </td>
                            <td>
                                <a target="_blank" href="https://${ethereum_network}.etherscan.io/tx/${transactionHash}">Show it on ${ethereum_network} (${shortenedHash}).</a>
                            </td>
                            <td>
                                The Manufacturer has received the production order.
                            </td>`
    ).removeClass('table').addClass('table-success');
}

function handlePrintStarted(state) {
    let transactionHash = state.transactionHash;
    console.log(transactionHash);

    let shortenedHash = hashShortener(transactionHash);

    setProgress(80);

    $('#stateThreeTable').html(`
                            <td>
                                3
                            </td>
                            <td>
                                Production Started
                            </td>
                            <td>
                                <a target="_blank" href="https://${ethereum_network}.etherscan.io/tx/${transactionHash}">Show it on ${ethereum_network} (${shortenedHash}).</a>
                            </td>
                            <td>
                                The generation process has been started.
                            </td>`
    ).removeClass('table').addClass('table-success');
}

function handlePrintDone(state, charts) {
    let transactionHash = state.transactionHash;
    console.log(transactionHash);

    let shortenedHash = hashShortener(transactionHash);

    setProgress(100);

    $('#stateFourTable').html(`
                            <td>
                                4
                            </td>
                            <td>
                                Production Done
                            </td>
                            <td>
                                <a target="_blank" href="https://${ethereum_network}.etherscan.io/tx/${transactionHash}">Show it on ${ethereum_network} (${shortenedHash}).</a>
                            </td>
                            <td>
                                The generation process has been finished.
                            </td>`
    ).removeClass('table').addClass('table-success');
    renderCharts(charts, state.printLogJson);
    setButtonStyle(false, "Print now!");
}

// state: {false, true}, text: set the text of the button
function setButtonStyle(state, text) {
    $('#fireProductionButton').prop("disabled", state).text(text);
}

function renderCharts(charts, rawSensorsData) {
    charts.collapse('show');
    console.log("Raw sensors data: ");
    console.log(rawSensorsData);
    let datasets = getMetalGearData(rawSensorsData);
    renderBedTemperature(datasets.bed);
    renderExtruderTemperature(datasets.extruder);
}

function renderBedTemperature(bedData) {
    var ctx1 = document.getElementById("myChart1").getContext('2d');
    var myChart1 = new Chart(ctx1, {
        type: 'line',
        data: {
            labels: bedData.layer,
            datasets: [{
                label: 'Target temperature',
                data: bedData.target,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }, {
                label: 'Sensor temperature',
                data: bedData.sensor,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        //beginAtZero: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        autoSkip: true,
                        maxTicksLimit: 20,
                        maxRotation: 90,
                        minRotation: 90
                    }
                }]
            }
        }
    });
}

function renderExtruderTemperature(extruderData) {
    var ctx2 = document.getElementById("myChart2").getContext('2d');
    var myChart2 = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: extruderData.layer,
            datasets: [{
                label: 'Target temperature',
                data: extruderData.target,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }, {
                label: 'Sensor temperature',
                data: extruderData.sensor,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        //beginAtZero: true
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        autoSkip: true,
                        maxTicksLimit: 20,
                        maxRotation: 90,
                        minRotation: 90
                    }
                }]
            }
        }
    });
}

function getMetalGearData(data) {
    let datasets = {bed: {target: [], sensor: [], layer: []}, extruder: {target: [], sensor: [], layer: []}};
    let layer = 0;
    data.forEach(item => {
        datasets.bed.target[layer] = item.bed.target;
        datasets.bed.sensor[layer] = item.bed.actual;
        datasets.bed.layer[layer] = "layer " + layer + " (" + item.time.substr(0,22) + ")";
        datasets.extruder.target[layer] = item.tool0.target;
        datasets.extruder.sensor[layer] = item.tool0.actual;
        datasets.extruder.layer[layer] = "layer " + layer + " (" + item.time.substr(0,22) + ")";
        layer++;
    });
    console.log(datasets);
    return datasets;
}

function capitalize(str) {
    str = str.toString();
    return str.charAt(0).toUpperCase() + str.slice(1);
}