const log = require('ololog').configure({time: true});


const devMode = true;
if(devMode === true) {
    log.red("⚠️  USING DEVELOPMENT MODE SETTINGS! ⚠️");
    require('dotenv').config({ path: '/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/dotenv-dev/.env' });
} else {
    log.blue("🚀 Using production settings 👌");
    require('dotenv').config({ path: '/home/pi/ManufacturerApp/.env' });
}


const Web3 = require('web3');
const Tx = require('ethereumjs-tx');
const fs = require('fs');

// connect to Infura node
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_ENDPOINT)); // FIXME

// the address that will send the test transaction
const addressFrom = process.env.WALLET_ADDRESS;
const privKey = process.env.WALLET_PRIVATE_KEY;

let contract_PrintProofOfWork = null;


function getSmartContract(address) {
    const contractJson = fs.readFileSync(process.env.SMART_CONTRACT_JSON, 'utf8'); // FIXME
    let contractAbi = JSON.parse(contractJson).abi;

    if (contract_PrintProofOfWork === null) {
        return new web3.eth.Contract(contractAbi, address, {
            from: web3.eth.defaultAccount,
            gasPrice: '200000000'
        });
    } else {
        return contract_PrintProofOfWork;
    }
}

// Signs the given transaction data and sends it. Abstracts some of the details
// of buffering and serializing the transaction for web3.
function sendSigned(txData, cb) {
    const privateKey = Buffer.from(privKey, 'hex');
    const transaction = new Tx(txData);
    transaction.sign(privateKey);
    const serializedTx = transaction.serialize().toString('hex');
    web3.eth.sendSignedTransaction('0x' + serializedTx).on('receipt', cb);
}


async function deploy(callback) {

    let nonce = await web3.eth.getTransactionCount(addressFrom);

    let gasLimit = JSON.parse(JSON.stringify(await web3.eth.getBlock("latest"))).gasLimit;
    let gasPrice = await web3.eth.getGasPrice();

    console.log("gas limit: " + gasLimit);
    console.log("gas price: " + gasPrice);

    let contractAbi = JSON.parse(fs.readFileSync(process.env.SMART_CONTRACT_JSON).toString()).bytecode;

    // construct the transaction data
    const txData = {
        nonce: web3.utils.toHex(nonce),
        //gasLimit: gasLimit, //web3.utils.toHex(25000),
        gasPrice: gasPrice, //web3.utils.toHex(10e9), // 10 Gwei
        from: addressFrom,
        data: '0x' + contractAbi
        //value: web3.utils.toHex(web3.utils.toWei('123', 'wei'))
    };

    let estimatedGas = await web3.eth.estimateGas(txData);
    console.log("estimated gas:" + estimatedGas);

    txData.gas = estimatedGas;

    // fire away!
    log.yellow.underline('⏰ Please wait...');
    sendSigned(txData, function (result) {
        callback(result);
    })
}

async function callSmartContractMethod(functionName, arguments, contractAddress, callback) {

    log.cyan(`🔮📜 Invoking ${functionName} with arguments: ${arguments} for the contract ${contractAddress}`);

    let nonce = await web3.eth.getTransactionCount(addressFrom);

    let gasLimit = JSON.parse(JSON.stringify(await web3.eth.getBlock("latest"))).gasLimit;
    let gasPrice = await web3.eth.getGasPrice();

    console.log("gas limit: " + gasLimit);
    console.log("gas price: " + gasPrice);

    switch (functionName) {
        case 'printOrderReceived': fs.writeFileSync(process.env.SHARED_NONCE_JSON, JSON.stringify({nonce: nonce}));
            break;
        case 'printStarted': {
            nonce = JSON.parse(fs.readFileSync(process.env.SHARED_NONCE_JSON).toString()).nonce;
            nonce++;
            fs.writeFileSync(process.env.SHARED_NONCE_JSON, JSON.stringify({nonce: nonce}));
        }
            break;
        case 'printFinished': {
            nonce = JSON.parse(fs.readFileSync(process.env.SHARED_NONCE_JSON).toString()).nonce;
            nonce++;
            fs.writeFileSync(process.env.SHARED_NONCE_JSON, JSON.stringify({nonce: nonce}));
        }
            break;
        default: break;
    }

    console.log("nonce: " + nonce);

    contract_PrintProofOfWork = getSmartContract(contractAddress);
    const contractFunction = contract_PrintProofOfWork.methods[functionName].apply(this, arguments);
    const functionAbi = contractFunction.encodeABI();


    // construct the transaction data
    const txData = {
        nonce: web3.utils.toHex(nonce),
        //gasLimit: 3000000, //web3.utils.toHex(25000),
        gasPrice: gasPrice, //web3.utils.toHex(10e9), // 10 Gwei
        //contractAddress: contractAddress,
        to: contractAddress,
        from: addressFrom,
        data: functionAbi
        //value: web3.utils.toHex(web3.utils.toWei('123', 'wei'))
    };

    let estimatedGas = await web3.eth.estimateGas(txData);
    console.log("estimated gas:" + estimatedGas);

    txData.gas = estimatedGas;

    log.yellow.underline('⏰ Please wait...');
    log.yellow(`☎️⛓ Calling smart contract function: ${functionName} with arguments: ${arguments} smart contract address is: ${contractAddress}`);

    // fire away!
    sendSigned(txData, function (result) {
        callback(result);
    })

}



//deploy(console.log);
//callSmartContractMethod('printRequested', ['0xa5B064B11640acBEd623D09Ac160B867d27f03ce'], '0x229359B6C1b106df7cF05F3AA89f9b5cE43c02c9', console.log);
//callSmartContractMethod('printOrderReceived', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce', console.log);
//callSmartContractMethod('printStarted', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce');
//allSmartContractMethod('printFinished', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce');

//callSmartContractMethod('printOrderReceived', ['0x0b33600c75cb047e81fdb846096e647f5cba47725dfdcaa50034eddb170765bc'], '0x0eA7C8a3e52a926180c2dA04abd6594f23b9736a', console.log);

module.exports = {callSmartContractMethod, deploy, web3};