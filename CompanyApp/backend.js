"use strict";
const log = require('ololog').configure({time: true});

const devMode = true;
if(devMode === true) {
    log.red("⚠️  USING DEVELOPMENT MODE SETTINGS!⚠️");
    require('dotenv').config({path: "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/CompanyApp/dev/.env"});
} else {
    log.blue("🚀 Using production settings 👌");
    require('dotenv').config({path: "/Users/sahelanthropus/IdeaProjects/ThesisPrototype/CompanyApp/.env"});
}


var request = require("request");
process.title = 'company-app-prototype';
let fs = require('fs');




const MANUFACTURER_HOST = process.env.MANUFACTURER_HOST;
const MANUFACTURER_PORT = process.env.MANUFACTURER_PORT;
var  webSocketsServerPort = process.env.WEBSOCKET_PORT;
var httpListeningPort = process.env.HTTP_PORT;

log.yellow("🚧  "+objName.call({MANUFACTURER_HOST})+": " + MANUFACTURER_HOST);
log.yellow("🚧  "+objName.call({MANUFACTURER_PORT})+": " + MANUFACTURER_PORT);
log.yellow("🚧  "+objName.call({webSocketsServerPort})+": " + webSocketsServerPort);
log.yellow("🚧  "+objName.call({httpListeningPort})+": " + httpListeningPort);
log.green("📦  Config settings loaded.");


// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');

var database = require('./database');
var deployer = require('./blockchain-functionalities');

var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');
var multiparty = require('multiparty');
var util = require('util');

var serve = serveStatic("./");

/**
 * Global variables
 */
// latest 100 messages
var history = [];
// list of currently connected clients (users)
var clients = [];


var parameters;

/**
 * HTTP backend (used only to load the HTML initial page for user GUI
 */
var backend = http.createServer(function (request, response) {
    if (request.url === '/') request.url = '/client.html';
    var done = finalhandler(request, response);
    serve(request, response, done);
}).listen(webSocketsServerPort, function () {
    log.cyan("CompanyApp frontend is listening on 👂 http://localhost:" + webSocketsServerPort + " (Websockets)");
});


/**
 * HTTP backend (used only to communicate with the ManufacturerApp)
 */
var express = require('express');
var httpServer = express();

httpServer.get('/', function (req, res) {
    res.send('Hello World!');
});

httpServer.post('/ping', function (req, res) {
    log.yellow("HTTP ping received");
    res.send('Hello World!');
    res.end();
    deployer.subscribeToBlockchainEvents(connection);
});

httpServer.post('/api/printStarted', function (req, res) {
    log.blue.underline("🎉 Print started! Transaction hash: " + req.query.transactionHash);
    res.send('Thank you!');
    res.end();
    sendData({type: "newState", state: {type: "3", transactionHash: req.query.transactionHash}});
});

httpServer.post('/api/printDone', function (req, res) {
    log.blue.underline("🎉 Print done! Transaction hash: " + req.query.transactionHash + " logHash: " + req.query.logHash);

    let transactionHash = req.query.transactionHash;
    var form = new multiparty.Form();

    form.parse(req, function (err, fields, files) {
        log.red("err " + util.inspect(err));
        log.red("fields " + util.inspect(fields));
        log.red("files " + util.inspect(files));

        res.send('Thank you!');
        res.end();

        let fileObject = files.file[0];
        let fileName = fileObject.originalFilename;
        let filePath = fileObject.path;
        let printLogJson = JSON.parse(fs.readFileSync(filePath).toString());

        log.green(printLogJson);

        let replacePart = {
            printLogJson: printLogJson,
            transactionHash: transactionHash,
            logHash: req.query.logHash,
            contractAddress: printLogJson.contractAddress,
            storedTime: Date()
        };

        database.addReplacedPart(replacePart);
        sendData({
            type: "newState",
            state: {type: "4", transactionHash: transactionHash, printLogJson: printLogJson.printLog}
        });

        pushHistoryDataToClient();
    });


});

httpServer.post('/api/orderReceived', function (req, res) {
    log.blue.underline("🎉 Order received! Transaction hash: " + req.query.transactionHash);
    res.send('Thank you!');
    res.end();
    sendData({type: "newState", state: {type: "2", transactionHash: req.query.transactionHash}});
});

httpServer.listen(httpListeningPort, function () {
    log.cyan('CompanyApp backend is listening on 👂 http://localhost:' + httpListeningPort + " (HTTP)");
});

/**
 * WebSocket backend
 */
var wsServer = new webSocketServer({
    // WebSocket backend is tied to a HTTP backend. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: backend
});

var connection;
var contractAddress = null;
// This callback function is called every time someone
// tries to connect to the WebSocket backend
wsServer.on('request', function (request) {
    log.cyan('New connection from 👀 ' + request.origin + ' established');

    // accept connection - you should check 'request.origin' to make sure that
    // client is connecting from your website
    // (http://en.wikipedia.org/wiki/Same_origin_policy)
    connection = request.accept(null, request.origin);
    // we need to know client index to remove them on 'close' event
    var index = clients.push(connection) - 1;
    var userName = false;
    var userColor = false;

    pushParametersDataToClient();
    pushHistoryDataToClient();

    //sendData({"type":"eventFromSmartContract","eventData":{"0":true,"1":true,"2":null,"3":null,"4":"0xb7296cba4162e46fffd935c420f87104defb0453445aa20b891f454ebcf0ce2a","5":"0xb7296cba4162e46fffd935c420f87104defb0453445aa20b891f454ebcf0ce2a","6":"0x0000000000000000000000000000000000000000000000000000000000000000","7":"0x0000000000000000000000000000000000000000000000000000000000000000","b_printRequested":true,"b_printOrderReceived":true,"b_printStarted":null,"b_printFinished":null,"gcodeHashCompany":"0xb7296cba4162e46fffd935c420f87104defb0453445aa20b891f454ebcf0ce2a","gcodeHashReceived":"0xb7296cba4162e46fffd935c420f87104defb0453445aa20b891f454ebcf0ce2a","gcodeHashPrinter":"0x0000000000000000000000000000000000000000000000000000000000000000","printLogHash":"0x0000000000000000000000000000000000000000000000000000000000000000"}} )

    // user sent some message
    connection.on('message', function (msg) {
        var message = JSON.parse(msg.utf8Data);
        log.yellow.underline(`💌 Received a print request from frontend: ${message}`);
        if (message.type === 'printRequest') {
            database.getParameterByPartNumber(parseInt(message.partNumber), (data) => {
                log.magenta("💿 MongoDB [Design Production Repository] data identifier: " + data._id);
                // to write gcode on file please use: fs.writeFile(filePath, data.gcode.file_data,  "binary",function(err) { });
                log.green.underline("🔨⛓ Deploying the smart contract. Please, wait...");
                deployer.deploy((deploymentReceipt) => {
                    console.log(deploymentReceipt);
                    contractAddress = deploymentReceipt.contractAddress;
                    deployer.subscribeToBlockchainEvents(contractAddress, sendEventData);
                    sendData({type: "newState", state: {type: "0", contractAddress: contractAddress}});

                    fs.writeFileSync("./tmp.gcode", data.gcode.file_data, "binary");
                    let file = fs.readFileSync("./tmp.gcode");
                    log.magenta("🔑 Hash of the GCODE to be sent: " + deployer.web3.utils.sha3(file));
                    log.green.underline("🔥⛓ Transacting [printRequested]. Please, wait...");
                    deployer.callSmartContractMethod('printRequested', [deployer.web3.utils.sha3(file)], contractAddress, (transactionReceipt) => {
                        console.log("transaction receipt: ");
                        log.blue("🔮 Transaction of [printRequested] is done, see it on " + process.env.ETHEREUM_NETWORK + " at: https://" + process.env.ETHEREUM_NETWORK + ".etherscan.io/tx/" + transactionReceipt.transactionHash);
                        sendData({type: "newState", state: {type: "1", transactionHash: transactionReceipt.transactionHash}});
                        sendGcodeToManufacturerApp({
                            gcodeName: data.gcodeName,
                            partNumber: data.partNumber,
                            contractAddress: contractAddress
                        });
                    })
                });
            });
        } if(message.type === 'temepratureLogRequest') {
            log.blue("Requested temperature log for the SC: " + message.contractAddress);
            pushTemperaturesToClient(message.contractAddress);
        } else {
            log.red.underline("[error] Message type not recognized.");
        }
        //connection.send(JSON.stringify({response: "success"}));
    });

    // user disconnected
    connection.on('close', function (connection) {
        if (userName !== false && userColor !== false) {
            log.red.underline((new Date()) + "😔 Peer "
                + connection.remoteAddress + " disconnected.");
            // remove user from the list of connected clients
            clients.splice(index, 1);
        }
    });


    function pushParametersDataToClient() {
        database.getAllPartNumberObjects(function (result) {
            result.forEach(function (param) {
                delete param._id;
                delete param.gcode;
                delete param.stl;
            });
            sendData({
                type: "parametersFromDatabase",
                parameters: result,
                ethereum_network: process.env.ETHEREUM_NETWORK
            });
            log.magenta.underline("📀 Parameters data loaded from [Design Production Repository]:");
            //log.magenta(result);
        });
    }
});

function pushHistoryDataToClient() {
    database.getHistoryFromDB(function (result) {
        //console.log(result);
        sendData({
            type: "historyFromDatabase",
            entries: result,
            ethereum_network: process.env.ETHEREUM_NETWORK
        });
        log.magenta.underline("📀 Entries from [Part History Repository] are loaded");
        //log.magenta(result);
    });
}

function pushTemperaturesToClient(contractAddress) {
    database.getTemperaturesFromDBbySC(contractAddress, function (result) {
        //console.log(result);
        sendData({
            type: "temperaturesLog",
            data: {contractAddress: contractAddress, temperatures: result /*printLogJson.printLog*/}
        });
        log.magenta.underline("📀 Temperatures data loaded from [Part History Repository]:");
        //log.magenta(result);
    });
}

function sendData(jsonobject) {
    var stringifiedJson = JSON.stringify(jsonobject);
    connection.sendUTF(stringifiedJson);
    log.yellow("💌 The message [" + jsonobject.type + "] has been sent to the client.");
}

function sendEventData(eventObject) {
    sendData({type: "eventFromSmartContract", eventData: eventObject.returnValues});
}

function sendGcodeToManufacturerApp(data) {

    var options = {
        method: 'POST',
        url: 'http://' + MANUFACTURER_HOST + ':' + MANUFACTURER_PORT + '/api/print',
        headers:
            {
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
            },
        formData:
            {
                file:
                    {
                        value: fs.createReadStream("./tmp.gcode"),
                        options:
                            {
                                filename: './' + data.gcodeName,
                                contentType: null
                            }
                    },
                contractAddress: data.contractAddress,
                gcodeName: data.gcodeName,
                partNumber: data.partNumber,
            }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        log.green("✈️ GCODE file of the part number [" + data.partNumber + "] has been sent to ManufacturerApp via HTTP");
    });

}


/*

if (message.type === 'utf8') { // accept only text
    if (userName === false) { // first message sent by user is their name
        // remember user name
        userName = htmlEntities(message.utf8Data);
        // get random color and send it back to the user
        userColor = colors.shift();
        connection.sendUTF(JSON.stringify({ type:'color', data: userColor }));


    } else { // log and broadcast the message


        // we want to keep history of all sent messages
        var obj = {
            time: (new Date()).getTime(),
            text: htmlEntities(message.utf8Data),
            author: userName,
            color: userColor
        };
        history.push(obj);
        history = history.slice(-100);

        // broadcast message to all connected clients
        var json = JSON.stringify({ type:'message', data: obj });
        for (var i=0; i < clients.length; i++) {
            clients[i].sendUTF(json);
        }
    }
} else if (message.type === 'json') {
    log("received a JSON Object: " + message);
} else {
    log("Unknown message type: " + message);
}

*/

function simulatePrintDone() {
    log.blue.underline("🎉 Print done! Transaction hash: ");

    let transactionHash = "FAKE_TRANSACTION_HASH_99999";
    let printLogJson = JSON.parse(fs.readFileSync("/Users/sahelanthropus/IdeaProjects/ThesisPrototype/CompanyApp/test.log.json").toString());

    log.green(printLogJson);

    let replacePart = {
        printLogJson: printLogJson,
        transactionHash: transactionHash,
        smartContractAddress: contractAddress,
        storedTime: Date()
    };

    database.addReplacedPart(replacePart);
    /*sendData({
        type: "newState",
        state: {type: "4", transactionHash: transactionHash, printLogJson: printLogJson.printLog}
    });*/

}

//simulatePrintDone();

function objName() {
    return Object.keys(this)[0];
}
