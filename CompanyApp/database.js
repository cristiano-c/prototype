var mongo = require('mongodb');
var fs = require('fs');
var path = require('path'); // Used to extract file name from full paths

var MongoClient = require('mongodb').MongoClient;
var Binary = require('mongodb').Binary; // Converts large files to binary format that will be stored in MongoDB
var url = "mongodb://localhost:27017/"; // Configure the url with your already running mongodb instance url

var options = {useNewUrlParser: true};

var DESIGN_PRODUCTION_REPOSITORY_DB_NAME = "DesignProductionRepository";
var PARAMETERS_COLLECTION_NAME = "Parameters";
var HISTORY_DB = "PartHistoryRepository";

var PART_HISTORY_REPOSITORY_DB_NAME = "PartHistoryRepository";
var PART_HISTORY_COLLECTION_NAME = "ReplacedPart";

// Reusable function to create a new collection into the specified DB
function createCollection(designdb, newCollectionName) {

    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(designdb);
        dbo.createCollection(newCollectionName, function (err, res) {
            if (err) throw err;
            console.log("Collection " + newCollectionName + " created!");
            db.close();
        });
    });

}

// Reusable function to add some entry in the specified DB and collection
function addEntry(dbname, collection, objectToInsert) {

    // Store the binary data into MongoDB into specified collection
    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(dbname);
        var myobj = objectToInsert;
        dbo.collection(collection).insertOne(myobj, function (err, res) {
            if (err) throw err;
            console.log("New entry inserted!");
            db.close();
        });
    });
}

// Run once to create DB (to be used only as an example!)
function createDesignDB() {
    var parameter = null;
    var filePath = null;
    createCollection(DESIGN_PRODUCTION_REPOSITORY_DB_NAME, "Parameters");

    filePath = "/Users/sahelanthropus/IdeaProjects/Prototype/Company/GcodeSamples/heart.gcode";
    parameter = {partNumber: 1, gcode: getBinaryDataFromFile(filePath), gcodeName: path.basename(filePath)};
    addEntry(DESIGN_PRODUCTION_REPOSITORY_DB_NAME, PARAMETERS_COLLECTION_NAME, parameter);

    filePath = "/Users/sahelanthropus/IdeaProjects/Prototype/Company/GcodeSamples/simplecircle.gcode";
    parameter = {partNumber: 2, gcode: getBinaryDataFromFile(filePath), gcodeName: path.basename(filePath)};
    addEntry(DESIGN_PRODUCTION_REPOSITORY_DB_NAME, PARAMETERS_COLLECTION_NAME, parameter);

    filePath = "/Users/sahelanthropus/IdeaProjects/Prototype/Company/GcodeSamples/xyzCalibration_cube.gcode";
    parameter = {partNumber: 3, gcode: getBinaryDataFromFile(filePath), gcodeName: path.basename(filePath)};
    addEntry(DESIGN_PRODUCTION_REPOSITORY_DB_NAME, PARAMETERS_COLLECTION_NAME, parameter);
}

// Per aggiungere un nuovo file al DB, cambiare il path nella riga sottostante e avviare node database.js
// filePath = "/Users/sahelanthropus/stampa_3d/modelli_3d/Fine_small_ring.gcode";
// parameter = { partNumber: 777, gcode: getBinaryDataFromFile(filePath), gcodeName: path.basename(filePath)};
// addEntry(DESIGN_DB_NAME, PARAMETERS_COLLECTION_NAME,parameter);


/*
let files = [
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/Buick_keychain.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/Cadillac_keychain.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/Chevrolet_keychain.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/GMC_keychain.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/GMC_yukon_keychain.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/GM_bottle_opener.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/heat_exchanger.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/piston.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/profile.gcode",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/GCODE/wheel.gcode"
];
let stls = [
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/Buick_keychain.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/Cadillac_keychain.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/Chevrolet_keychain.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/GMC_keychain.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/GMC_yukon_keychain.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/GM_bottle_opener.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/heat_exchanger.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/piston.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/profile.stl",
    "/Users/sahelanthropus/Desktop/labhwpolito/FINAL/STL/wheel.stl"
];
let i = 0;
files.forEach(filepath => {
    parameter = {
        partNumber: (parseInt(`${i}${i}${(i+2)}${(i + 3)}`)),
        gcode: getBinaryDataFromFile(filepath),
        gcodeName: path.basename(filepath),
        stl: getBinaryDataFromFile(stls[i]),
        stlName: path.basename(stls[i])
    };
    addEntry(DESIGN_PRODUCTION_REPOSITORY_DB_NAME, PARAMETERS_COLLECTION_NAME, parameter);
    i++;
});
*/


// Read file content and save it binary format into 'insert_data'
function getBinaryDataFromFile(filePath) {
    var data = fs.readFileSync(filePath);
    var binaryFileData = {};
    binaryFileData.file_data = Binary(data);
    return binaryFileData;
}

// Use once to create the DB!
//createDesignDB();

// Run once to create DB
function getParameterByPartNumber(partNumber, callback) {

    // Store the binary data into MongoDB into specified collection
    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(DESIGN_PRODUCTION_REPOSITORY_DB_NAME);
        dbo.collection(PARAMETERS_COLLECTION_NAME).find({partNumber: partNumber}).toArray(function (err, data) {
            // console.log(data); // Returns an array with a single object
            db.close();
            if (callback != null || callback !== undefined) callback(data[0]);
        });
    });
}

// Get binary data from a 'parameter' object
function getBinaryDataFromParameter(parameter) {
    return parameter.gcode.file_data;
}

// Write some binary data to a file
function writeBinaryDataToFile(binaryData, filePath) {
    console.log("Writing binary data to:" + filePath);
    //console.log(binaryData);
    fs.writeFile(filePath, binaryData, "binary", function (err) {
    });
}

// Only for test, debug and examples!
var filePath = "/Users/sahelanthropus/IdeaProjects/Prototype/Company/GcodeSamples/void.gcode";
//getParameterByPartNumber(1, parameter => {writeBinaryDataToFile(getBinaryDataFromParameter(parameter), filePath)});

// Example how to use 'getParameterByPartNumber' using callbacks
// getParameterByPartNumber(123, console.log);

async function MYgetParameterByPartNumber(partNumber) {

    // Store the binary data into MongoDB into specified collection
    return MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(DESIGN_PRODUCTION_REPOSITORY_DB_NAME);
        return dbo.collection(PARAMETERS_COLLECTION_NAME).find({partNumber: partNumber}).toArray(function (err, data) {
            // console.log(data); // Returns an array with a single object
            db.close();
            return data[0];
        });
    });
}


function getAllPartNumberObjects(callback) {

    // Store the binary data into MongoDB into specified collection
    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(DESIGN_PRODUCTION_REPOSITORY_DB_NAME);
        dbo.collection(PARAMETERS_COLLECTION_NAME).find().toArray(function (err, data) {
            // console.log(data); // Returns an array with a single object
            db.close();
            if (callback != null || callback !== undefined) callback(data);
        });
    });
}

function addReplacedPart(printLog) {
    addEntry(PART_HISTORY_REPOSITORY_DB_NAME, PART_HISTORY_COLLECTION_NAME, printLog);
}

function getHistoryFromDB(callback) {

    // Store the binary data into MongoDB into specified collection
    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(PART_HISTORY_REPOSITORY_DB_NAME);
        dbo.collection(PART_HISTORY_COLLECTION_NAME).find().toArray(function (err, data) {
            db.close();
            if (callback != null || callback !== undefined) callback(data);
        });
    });
}

function getTemperaturesFromDBbySC(contractAddress, callback) {

    // Store the binary data into MongoDB into specified collection
    MongoClient.connect(url, options, function (err, db) {
        if (err) throw err;
        var dbo = db.db(PART_HISTORY_REPOSITORY_DB_NAME);
        dbo.collection(PART_HISTORY_COLLECTION_NAME).find({'printLogJson.contractAddress': contractAddress}).toArray(function (err, data) {
            db.close();
            if (callback != null || callback !== undefined) callback(data[0].printLogJson.printLog);
        });
    });
}

module.exports = {getAllPartNumberObjects, getParameterByPartNumber, addReplacedPart, getHistoryFromDB, getTemperaturesFromDBbySC};