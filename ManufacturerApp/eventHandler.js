const log = require('ololog').configure({time: true});

const devMode = true;
if (devMode === true) {
    log.red("⚠️  USING DEVELOPMENT MODE SETTINGS! ⚠️");
    require('dotenv').config({path: '/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/dotenv-dev/.env'});
} else {
    log.blue("🚀 Using production settings 👌");
    require('dotenv').config({path: '/home/pi/ManufacturerApp/.env'});
}


const web3 = require('web3');
const express = require('express');
const Tx = require('ethereumjs-tx');
const fs = require('fs');
const request = require('request');
const app = express();
const ip = require('ip');
const formidable = require('formidable');
const http = require('http');
const util = require('util');
const net = require('net');
const busboy = require('connect-busboy'); //middleware for form/file upload
const path = require('path');     //used for file path
const OCTOPRINT_PORT = process.env.OCTOPRINT_PORT; //80;
const OCTOPRINT_HOST = process.env.OCTOPRINT_HOST;
const OCTOPRINT_API_KEY = process.env.OCTOPRINT_API_KEY;
const MANUFACTURER_LOG_FOLDER = process.env.MANUFACTURER_LOG_FOLDER;
const OCTOPRINT_GCODES_FOLDER = process.env.OCTOPRINT_GCODES_FOLDER;
const PRINT_LOG_JSON_FILE = process.env.MANUFACTURER_LOG_FOLDER + 'print.log.json';
const COMPANY_HOST = process.env.COMPANY_HOST;
const COMPANY_PORT = process.env.COMPANY_PORT;
var ORIGINAL_IP = null;
const blockchain = require('./blockchain-functionalities');

//var access = fs.createWriteStream(MANUFACTURER_LOG_FOLDER + 'events.log');
//process.stdout.write = process.stderr.write = access.write.bind(access);

const SHARED_CONTRACT_ADDRESS = process.env.SHARED_CONTRACT_ADDRESS;
const SHARED_PART_NUMBER = process.env.SHARED_PARTNUMBER;
const SHARED_GCODE_NAME = process.env.SHARED_GCODE_NAME;

const EVENT_TYPE = process.argv[2];
const PRINT_START_TIME = process.argv[3];
const PRINT_FILE_NAME = process.argv[4];
const contractAddress = JSON.parse(fs.readFileSync(SHARED_CONTRACT_ADDRESS).toString()).contractAddress;

eventDispatcher(EVENT_TYPE);

function eventDispatcher(_eventType) {
    console.log("[0] Catched an event.");
    console.log("[1] Event type: " + _eventType);

    switch (_eventType) {
        case "connected":
            handlePrinterConnected();
            break;
        case "printStarted":
            handlePrintStarted();
            break;
        case "printDone":
            handlePrintDone();
            break;
        case "newLayer":
            handleNewLayer();
            break;
        default:
            handleNotRecognizedEvent();
    }
}

function handleNewLayer() {
    const TIME = process.argv[3];
    console.log("[2] Date: " + TIME);
    var printLogJson = JSON.parse(fs.readFileSync(PRINT_LOG_JSON_FILE));

    // TODO aggiungere dati dei sensori dentro a printLogJson

    getSensorsData(function (temperature) {
        temperature.time = TIME;
        printLogJson.printLog[printLogJson.printLog.length] = temperature;
        fs.writeFileSync(PRINT_LOG_JSON_FILE, JSON.stringify(printLogJson), {encoding: 'utf8'});
        process.exit();
    });
}

function handlePrintDone() {
    const TIME = process.argv[3];
    const FILE_NAME = process.argv[4];
    const FILE_PATH = process.argv[5];
    const ELAPSED_TIME = process.argv[6];
    console.log("[2] Date: " + TIME);
    console.log("[3] File name: " + FILE_NAME);
    console.log("[4] File path: " + FILE_PATH);
    console.log("[4] Elapsed time: " + ELAPSED_TIME);
    var printLogJson = JSON.parse(fs.readFileSync(PRINT_LOG_JSON_FILE).toString());
    var gcodeName = JSON.parse(fs.readFileSync(SHARED_GCODE_NAME).toString()).gcodeName;
    var partNumber = JSON.parse(fs.readFileSync(SHARED_PART_NUMBER).toString()).partNumber;
    printLogJson.timeElapsed = ELAPSED_TIME;
    printLogJson.dateDoneEvent = TIME;
    printLogJson.gcodeName = gcodeName;
    printLogJson.partNumber = partNumber;
    printLogJson.contractAddress = contractAddress;
    fs.writeFileSync(PRINT_LOG_JSON_FILE, JSON.stringify(printLogJson));
    var printLogJsonFile = fs.readFileSync(PRINT_LOG_JSON_FILE); // Okay, it could be a ugly solution and it has to be reworked (read, write, read, write, ...)

    // TODO: effetturare qui la transazione di stampa finita e caricare su blockchain: print.log.json {elapsedTime: 123..., <json object from OctoPrint about sensors data>}
    console.log("[5] Transaction about print done.");

    var logHash = blockchain.web3.utils.sha3(printLogJsonFile);
    blockchain.callSmartContractMethod('printFinished', [logHash], contractAddress, function (receipt) {
        console.log("Print done has been transacted");
        console.log("transaction done, see it on " + process.env.ETHEREUM_NETWORK + " at: https://" + process.env.ETHEREUM_NETWORK + ".etherscan.io/tx/" + receipt.transactionHash);
        var filepath = process.env.PRINT_LOG_JSON;
        sendFileToCompany(filepath, receipt.transactionHash, logHash);
    });
}

function handlePrintStarted() {
    const TIME = process.argv[3];
    const FILE_NAME = process.argv[4];
    const FILE_PATH = process.argv[5];
    log.yellow("[2] Date: " + TIME);
    log.yellow("[3] File name: " + FILE_NAME);
    log.yellow("[4] File path: " + FILE_PATH);
    log.yellow("[5] Reading gcode: " + OCTOPRINT_GCODES_FOLDER + FILE_PATH);

    var file = fs.readFileSync(OCTOPRINT_GCODES_FOLDER + FILE_PATH);

    fs.openSync(PRINT_LOG_JSON_FILE, 'w');
    fs.unlinkSync(PRINT_LOG_JSON_FILE);
    fs.writeFileSync(PRINT_LOG_JSON_FILE, JSON.stringify({
        PartNumber: FILE_NAME,
        dateStartedEvent: TIME,
        printLog: []
    }));

    // TODO: effetturare qui la transazione di stampa iniziata e calcolare proprio qui l'hash del file utilizzato
    log.yellow("[6] Making transaction about print started.");
    log.yellow("[7] Contract address is: " + contractAddress);

    blockchain.callSmartContractMethod('printStarted', [blockchain.web3.utils.sha3(file)], contractAddress, function (receipt) {
        log.blue("🥳⛓ Print started has been transacted. See it on " + process.env.ETHEREUM_NETWORK + " at: https://" + process.env.ETHEREUM_NETWORK + ".etherscan.io/tx/" + receipt.transactionHash);
        var data = {transactionHash: receipt.transactionHash};
        feedbackToCompanyApp(data, "/api/printStarted");
    });

}

function handlePrinterConnected() {
    const TIME = process.argv[3];
    const PORT = process.argv[4];
    const BAUDRATE = process.argv[5];
    log.yellow("[2] Date: " + TIME);
    log.yellow("[3] Port: " + PORT);
    log.yellow("[4] Baudrate: " + BAUDRATE);
    process.exit();
}

function handleNotRecognizedEvent(_eventType) {
    log.yellow("[0] Catched an event.");
    log.yellow("[1] Event type: " + _eventType);
    log.yellow("[3] This event is not handled. Please contact administrator.");
    process.exit();
}


// Not a final solution! This is only a temporary fix
// Each time OctoPrint event is fired, a NodeJS application remains open
// I trust that a simple transaction should not take more than 10 minutes
// After 10 minutes this script is launched, he kills himself with this setTimeout


function getSensorsData(callback) {
    var request = require("request");

    var options = {
        method: 'GET',
        url: 'http://' + OCTOPRINT_HOST + ':' + OCTOPRINT_PORT + '/api/printer',
        headers:
            {
                'cache-control': 'no-cache',
                'x-api-key': OCTOPRINT_API_KEY
            }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var temperature = JSON.parse(body).temperature;
        callback(temperature);
    });
}

function feedbackToCompanyApp(_jsonData, _apiUrl) {
    var request = require("request");

    var options = {
        method: 'POST',
        url: 'http://' + COMPANY_HOST + ':' + COMPANY_PORT + _apiUrl,
        qs: _jsonData,
        headers:
            {
                'cache-control': 'no-cache',
                'Access-Control-Allow-Headers': 'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, Accept-Encoding, X-GitHub-OTP, X-Requested-With, User-Agent'
            }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        log.green("🚗 Data sent back to the CompanyApp");
        process.exit();
    });
}

function sendFileToCompany(filepath, transactionHash, logHash) {
    var options = {
        method: 'POST',
        url: 'http://' + process.env.COMPANY_HOST + ':8000/api/printDone',
        qs: {transactionHash: transactionHash, logHash: logHash},
        headers:
            {
                'cache-control': 'no-cache',
                'Access-Control-Allow-Headers': 'Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, Accept-Encoding, X-GitHub-OTP, X-Requested-With, User-Agent',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
            },
        formData:
            {
                file:
                    {
                        value: fs.createReadStream(filepath),
                        options:
                            {
                                filename: filepath,
                                contentType: null
                            }
                    }
            }
    };


    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        log.cyan("📦📡 The print log file has been sent to the CompanyApp.");
        log.cyan("[DEBUG]: " + transactionHash + " " + logHash);
        process.exit();
    });
}

//sendFileToCompany('/Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp/shared/data.json');
