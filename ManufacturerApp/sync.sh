#! /usr/local/bin/bash

echo "syncing, please wait..."
rsync --exclude=.DS_Store --exclude=node_modules --exclude=sync.sh -a /Users/sahelanthropus/IdeaProjects/ThesisPrototype/ManufacturerApp pi@130.192.5.236:/home/pi
scp config.yaml pi@130.192.5.236:/home/pi/.octoprint/config.yaml
echo -n "synced!"