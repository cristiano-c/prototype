# 1 ThesisPrototype
ThesisPrototype è il sistema dimostrativo per il progetto di Connected Manufacturing in cui si dimostra come si può integrare la tecnologia blockchain con la stampa 3D.

# 2 Introduzione
Il sistema è interamente basato su NodeJS, suddiviso in due moduli:

- CompanyApp: va installata su un qualsiasi PC nel quale sia installato NodeJS
- ManufacturerApp: va installato sul Raspberry che si interfaccia con la stampante, il Raspberry dovrebbe avere OctoPrint e NodeJS

# 2 Guida all'installazione
In questa sezione viene spiegato come *installare* il sistema. Nella sezione successiva (\#3) viene spiegato come *configurarlo*.

## 2.1 Installazione di ManufacturerApp
Attualmente (luglio 2019) il modulo ManufacturerApp risulta correttamente installato sul Raspberry.

## 2.2 Installazione di CompanyApp
Scaricare l'intera repository [https://gitlab.com/cristiano-c/prototype](https://gitlab.com/cristiano-c/prototype/edit/master/README.md) e fare git checkout su master (default).

Dal terminale, posizionarsi al'interno della cartella ./ManufacturerApp e avviare l'installazione dei pacchetti NodeJS con: `npm install`.

A questo punto il modulo CompanyApp risulta essere installato.

⚠️ ATTENZIONE ⚠️
Si consiglia di utilizzare la versione di NodeJS v8 che è quella utilizzata durante lo sviluppo del sistema (la versione attuale –luglio 2019– risulta essere la v12).

# 3 Guida alla configurazione
È la parte che richiede un lavoro più attento. Il sistema è stato strutturato in modo tale che potesse essere facilmente configurabile tramite i file di configurazione `.env` di NodeJS.

Ce ne sono due:

- `./CompanyApp/.env`
- `./ManufacturerApp/.env`

Questa sezione guiderà l'utente alla corretta configurazione dei due moduli, per ogni modulo viene allegato il relativo `.env` **di esempio**. Si noti che i parametri presentati in questo documento **NON** sono validi per una corretta confiurazione, perchè dipendono dai parametri della rete (rete del Politecnico, IP del Raspberry e IP del PC) e dal Token INFURA.

Quindi, prima di pricedere, si annotino:

1. Token INFURA (registrarsi su [INFURA](https://infura.io/), creare un nuovo progetto e ottenere un nuovo Token)
2. IP del Raspberry
3. IP del PC

Nota: i wallet della Company e del Manufacturer sono attualmente hardcoded (pub\_key + pri\_key) nei due `.env` e non è necessario (a scopi di test) riconfigurarli, ma si ricordi nelle successive fasi di sviluppo di utilizzare nuovi Wallets! I walltes attuali sono stati gentilmente concessi dall'autore ai fini dello sviluppo in cui:

- Mainnet: NON ci sono Ether
- Ropsten: ci sono Ether
- Rinkeby: ci sono Ether
- Kovan: ci sono Ether

⚠️ ATTENZIONE! ⚠
Questa repository è PRIVATA e dovrebbe essere mantenuta PRIVATA! Le chiavi pubbliche e private dei due wallets sono reperibili nei due files `.env`. Si gestiscano con cura queste informazioni. L'autore non si assume nessuna responsaiblità di un uso improprio o di eventuali furti di fondi (sia mainnet che testnets).


⚠️ ATTENZIONE! ⚠
Il Token INFURA qui pubblicato era un Token valido, è stato lasciato come esempio di riferimento, ma **NON** è funzionate: ci si ricordi di sostituirlo con un nuovo Token INFURA.

# 3.1 Configurazione di ManufacturerApp
Attualmente la configurazione di `./ManufacturerApp/.env` risulta essere sufficiente per un corretto funzionamento del sistema, a parte il settaggio del token INFURA e degli indirizzi IP.

Il seguente listato mostra l'attuale (luglio 2019) configurazione del `./ManufacturerApp/.env`:

```
INFURA_ACCESS_TOKEN=ee2a7de4c7ba4c218f44b585c837f8cd
ETHEREUM_NETWORK=ropsten
INFURA_ENDPOINT=https://ropsten.infura.io/ee2a7de4c7ba4c218f44b585c837f8cd
WALLET_ADDRESS=0xddcf0fec78b5a20b9138f5ddb1357e3b60026a67
WALLET_PRIVATE_KEY=BDD2DAE470D3CE0BE97DBE8835EB91A5A6BD61DFC9767C3222AF4E32CE05E23F
DESTINATION_WALLET_ADDRESS=0x8882528c7104e146e0500203c353c09922575385
SMART_CONTRACT_ADDRESS=MustBeReceivedFromLocalPC
USING_RASPBERRY=true
LISTENING_PORT=8080
OCTOPRINT_PORT=5000
OCTOPRINT_HOST="localhost"
OCTOPRINT_USER="Polito3DPrint"
OCTOPRINT_PASS="vb645g-fdghnj5g-dfghfj-k9oi876hSDF"
INFURA_HTTPS_TOKEN="https://ropsten.infura.io/v3/82ab725d0f624783b1b5a70bdc7d4fbb"
OCTOPRINT_API_KEY="CA31AE04BBDF4258880842CDE9B8E19F"
MANUFACTURER_LOG_FOLDER="/home/pi/ManufacturerApp/logs/"
COMPANY_HOST="192.168.16.65"
COMPANY_PORT="8000"
SHARED_CONTRACT_ADDRESS="/home/pi/ManufacturerApp/shared/data.json"
SHARED_GCODE_NAME="/home/pi/ManufacturerApp/shared/gcodeName.json"
SHARED_PARTNUMBER="/home/pi/ManufacturerApp/shared/partNumber.json"
SMART_CONTRACT_JSON="/home/pi/ManufacturerApp/PrintProofOfWork.json"
SHARED_NONCE_JSON="/home/pi/ManufacturerApp/shared/nonce.json"
OCTOPRINT_GCODES_FOLDER="/home/pi/.octoprint/uploads/"
PRINT_LOG_JSON="/home/pi/ManufacturerApp/logs/print.log.json"
```

I parametri che vanno configurati sono:

```
INFURA_ACCESS_TOKEN=[nuovo Token INFURA]
ETHEREUM_NETWORK=[mainnet|ropsten|rinkeby|kovan] # Non è necessario cambiare subito i Wallets 
INFURA_ENDPOINT=https://ropsten.infura.io/[nuovo Token INFURA] # Attenzione alla rete di test! Meglio copiare questa URL dalla Dashboard di INFURA
INFURA_HTTPS_TOKEN="https://ropsten.infura.io/v3/82ab725d0f624783b1b5a70bdc7d4fbb" # Come sopra, purtroppo in questo momento non ricordo quale dei due non fosse più usato, nel dubbio per ora configurarli entrambi
COMPANY_HOST="[IP del PC]" # Deve essere raggiungile dalla rete del Poli!
```

# 3.2 Configurazione di CompanyApp
Attualmente la configurazione di `./Company/.env` risulta essere sufficiente per un corretto funzionamento del sistema, a parte il settaggio del token INFURA.

Il seguente listato mostra l'attuale (luglio 2019) configurazione del `./CompanyApp/.env`:

```
ETHEREUM_NETWORK=ropsten
MANUFACTURER_HOST="130.192.5.236"
MANUFACTURER_PORT="8080"
WEBSOCKET_PORT="1337"
HTTP_PORT="8000"
INFURA_ENDPOINT=https://ropsten.infura.io/ee2a7de4c7ba4c218f44b585c837f8cd
WALLET_ADDRESS=0x8882528C7104e146E0500203C353C09922575385
WALLET_PRIVATE_KEY=8E92643EC295A004104D97C416568A253C10430EEA5EE795DD836FA8E366566B
SMART_CONTRACT_JSON='/Users/sahelanthropus/IdeaProjects/ThesisPrototype/CompanyApp/PrintProofOfWork.json'
```

I parametri che vanno configurati sono:

```
INFURA_ACCESS_TOKEN=[nuovo Token INFURA]
ETHEREUM_NETWORK=[mainnet|ropsten|rinkeby|kovan] # Non è necessario cambiare subito i Wallets 
INFURA_ENDPOINT=https://ropsten.infura.io/[nuovo Token INFURA] # Attenzione alla rete di test! Meglio copiare questa URL dalla Dashboard di INFURA
INFURA_HTTPS_TOKEN="https://ropsten.infura.io/v3/82ab725d0f624783b1b5a70bdc7d4fbb" # Come sopra, purtroppo in questo momento non ricordo quale dei due non fosse più usato, nel dubbio per ora configurarli entrambi
MANUFACTURER_HOST="[IP del Raspberry]" # Deve essere raggiungile dalla rete del Poli!
```

# 3.3 Configurazione del Database MongoDB
La configurazione del DB MongoDB richiede che MongoDB sia correttamente installato sul PC. Inoltre, i files dei modelli 3D devono essere già presenti nel DB. Di seguito viene descritta la procedura di messa in funzione del database.

Si apra il file `./CompanyApp/database.js` si verifichi che il parametro di configurazione di MongoDB sia coerente con l'istanza di MongoDB `var url = "mongodb://localhost:27017/";`, quindi evnetualmente sostiturire l'IP con quello corretto dell'istanza di MongoDB.

Per aggiungere i files dei modelli 3D ci sono i commenti che lo spiegano molto bene (i commenti si trovano all'interno del file `./CompanyApp/database.js`), alcune funzioni sono state commentate per sicurezza, ma all'occorrenza le funzioni richieste per aggiungere i modelli 3D al database vanno "uncommentate" e poi si esegua `node database.js`. Prima di procedere con l'esecuzione delle funzioni che aggiungono i modelli 3D al database bisogna leggere attentamente tutti i commenti presenti all'interno di `./CompanyApp/database.js` e procedere con cautela.

In ogni caso, nel seguente elenco è stilata una checklist di riferimento per aiutare l'utente durante il processo di aggiunta dei modelli 3D nel DB. Se le seguenti strutture fossero mancanti, ci sono due modi per costruirle, utilizzare Robo3T e aggiungerle manualmente oppure se si sa utilizzare lo script `./CompanyApp/database.js` con un po' di hacking non dovrebbe essere difficile automatizzare la creazione del DB:

1. Controllare che in MongoDB ci sia un *database* con il nome `DesignProductionRepository`
2. Controllare che nel *database* `DesignProductionRepository` ci sia la *collection* `Parameters`
3. Controllare che in MongoDB ci sia un *database* con il nome `PartHistoryRepository`
4. Controllare che nel *database* `DesignProductionRepository` ci sia la *collection* `ReplacedPart`

Per aggiungere uno o più modelli 3D, è necessario che tali modelli siano in formato `*.gcode` (almeno finchè si usa la MakerBot e OctoPrint) e poi cercare all'interno del file `database.js` le seguenti quattro righe:

```
// Per aggiungere un nuovo file al DB, cambiare il path nella riga sottostante e avviare node database.js
// filePath = "/Users/sahelanthropus/stampa_3d/modelli_3d/Fine_small_ring.gcode";
// parameter = { partNumber: 777, gcode: getBinaryDataFromFile(filePath), gcodeName: path.basename(filePath)};
// addEntry(DESIGN_DB_NAME, PARAMETERS_COLLECTION_NAME,parameter);
```

Togliere i commenti e modificare in modo opportuno il path a cui si trovano i modelli 3D.

Nota: si tenga presente che una entry `Parameters` è un oggetto JSON che richiede tre campi:

```
{
    partNumber: 777, /*Va inserito manualmente, deve essere diverso per ogni file, purtroppo durante lo sviluppo non c'era la necessità di inserire un trigger che inserisse un ID univoco ad ogni nuova entry*/
    gcode: getBinaryDataFromFile(filePath), /*Viene automaticamente aggiunto da una delle funzioni presenti in database.js*/
    gcodeName: path.basename(filePath)} /*Viene automaticamente preso dal nome del file*/
}
```

Ricapitolando, inserire un modello 3D nel DB significa utilizzare la funzione `addEntry(database, collection, parameter)` specificando il nome del Database MongoDB, il nome della Collection e l'oggetto `parameter` (quello con tre campi descritto sopra). Questo sistema di inserimento di nuovi modelli nel DB è di basso livello, purtroppo non c'è stato il tempo di implementare una funzionalità del tipo drag-and-drop che automatizzaze questa procedura, ma il sistema sotto al cofano è predisposto e ci sono tutti gli strumenti per farlo.

# 4 Esecuzione
In questa sezione viene spiegato come eseguire il sistema e renderlo pronto all'uso:

1. Prima di tutto si deve lanciare ManufacturerApp, quindi, dal Raspberry (collegandosi in remoto oppure con una tastiera fisica) posizionarsi sul `./ManufacturerApp` ed eseguire da terminale: `node backend.js`. Dovrebbe partire il log che conferma la corretta esecuzione del modulo Manufacturer.
2. Dal PC posizionarsi su `./CompanyApp` ed eseguire da terminale: `node backend.js`. Dovrebbero partire in automatico anche `frontend.js`, lo si può verificare controllando il log che verrà prodotto.

Dovrebbe essere possibile anche eseguire prima Company e poi Manufacturer, ma lo si sconsiglia perchè potrebbe portare a qualche problema di connessione.

È possibile controllare lo stato di avanzamento collegandosi a [http://[IP_Raspberry]:8080](http://[IP_Raspberry]:8080) per accedere alla schermata di OctoPrint.

# 5 Extra

In questa sezione vengono elencati possibili suggerimenti per migliorare il sistema:

- [ ] Implementare solamente il protocollo HTTP2 (WebSockets)
- [ ] Spostare la configurazione dell'IP di MongoDB da database.js a ./CompanyApp/.env
- [ ] Inserire un plugi-in nel front end che visualizzi sul browser il modello 3D selezionato
- [ ] Aggiungere notifiche push basate sugli eventi dello Smart Contract che si visualizzino come notifica OS
- [ ] Automatizzare l'aggiunta di nuovi file 3D utilizzando la GUI, le funzioni in database.js ci sono già, è sufficiente implementare una semplice funzionalità del tipo drag-and-drop nella GUI per importare i nuovi modelli 3D
- [ ] Implementare un'interfaccia simile ad un explorer blockchain che consenta di verificare lo storico dei DB passati (c'è già e funziona bene, ma è minimale, potrebbe essere migliorato)
- [ ] Implementare funzionalità di login/logout magari usando Metamask

