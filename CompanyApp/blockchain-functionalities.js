const log = require('ololog').configure({time: true});


require('dotenv').config({path: '.env'});


const Web3 = require('web3');
const Tx = require('ethereumjs-tx');
const fs = require('fs');

// connect to Infura node
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.INFURA_ENDPOINT)); // FIXME

// the address that will send the test transaction
const addressFrom = process.env.WALLET_ADDRESS;
const privKey = process.env.WALLET_PRIVATE_KEY;

let contract_PrintProofOfWork = null;


function getSmartContract(address) {
    const contractJson = fs.readFileSync(process.env.SMART_CONTRACT_JSON, 'utf8'); // FIXME
    let contractAbi = JSON.parse(contractJson).abi;

    if (contract_PrintProofOfWork === null) {
        return new web3.eth.Contract(contractAbi, address, {
            from: web3.eth.defaultAccount,
            gasPrice: '200000000'
        });
    } else {
        return contract_PrintProofOfWork;
    }
}

// Signs the given transaction data and sends it. Abstracts some of the details
// of buffering and serializing the transaction for web3.
function sendSigned(txData, cb) {
    const privateKey = Buffer.from(privKey, 'hex');
    const transaction = new Tx(txData);
    transaction.sign(privateKey);
    const serializedTx = transaction.serialize().toString('hex');
    web3.eth.sendSignedTransaction('0x' + serializedTx).on('receipt', cb);
}


async function deploy(callback) {

    let nonce = await web3.eth.getTransactionCount(addressFrom);

    let gasLimit = JSON.parse(JSON.stringify(await web3.eth.getBlock("latest"))).gasLimit;
    let gasPrice = await web3.eth.getGasPrice();

    console.log("gas limit: " + gasLimit);
    console.log("gas price: " + gasPrice);

    let contractAbi = JSON.parse(fs.readFileSync(process.env.SMART_CONTRACT_JSON).toString()).bytecode;

    // construct the transaction data
    const txData = {
        nonce: web3.utils.toHex(nonce),
        //gasLimit: gasLimit, //web3.utils.toHex(25000),
        gasPrice: gasPrice, //web3.utils.toHex(10e9), // 10 Gwei
        from: addressFrom,
        data: contractAbi
        //value: web3.utils.toHex(web3.utils.toWei('123', 'wei'))
    };

    let estimatedGas = await web3.eth.estimateGas(txData);
    console.log("estimated gas:" + estimatedGas);

    txData.gas = estimatedGas;

    // fire away!
    log.yellow.underline('⏰ Please wait...');
    sendSigned(txData, function (result) {
        callback(result);
    })
}

async function callSmartContractMethod(functionName, arguments, contractAddress, callback) {

    log.cyan(`🔮📜 Invoking ${functionName} with arguments: ${arguments} for the contract ${contractAddress}`);

    let nonce = await web3.eth.getTransactionCount(addressFrom);

    let gasLimit = JSON.parse(JSON.stringify(await web3.eth.getBlock("latest"))).gasLimit;
    let gasPrice = await web3.eth.getGasPrice();

    console.log("gas limit: " + gasLimit);
    console.log("gas price: " + gasPrice);
    console.log("nonce: " + nonce);

    switch (functionName) {
        case 'printOrderReceived': fs.writeFileSync(process.env.SHARED_NONCE_JSON, JSON.stringify({nonce: txCount}));
            break;
        case 'printStarted': {
            nonce = JSON.parse(fs.readFileSync(process.env.SHARED_NONCE_JSON).toString()).nonce;
            nonce++;
        }
            break;
        case 'printFinished': {
            nonce = JSON.parse(fs.readFileSync(process.env.SHARED_NONCE_JSON).toString()).nonce;
            nonce++;
        }
            break;
        default: break;
    }

    contract_PrintProofOfWork = getSmartContract(contractAddress);
    const contractFunction = contract_PrintProofOfWork.methods[functionName].apply(this, arguments);
    const functionAbi = contractFunction.encodeABI();


    // construct the transaction data
    const txData = {
        nonce: web3.utils.toHex(nonce),
        //gasLimit: 3000000, //web3.utils.toHex(25000),
        gasPrice: gasPrice, //web3.utils.toHex(10e9), // 10 Gwei
        //contractAddress: contractAddress,
        to: contractAddress,
        from: addressFrom,
        data: functionAbi
        //value: web3.utils.toHex(web3.utils.toWei('123', 'wei'))
    };

    let estimatedGas = await web3.eth.estimateGas(txData);
    console.log("estimated gas:" + estimatedGas);

    txData.gas = estimatedGas;

    log.yellow.underline('⏰ Please wait...');
    log.yellow(`☎️⛓ Calling smart contract function: ${functionName} with arguments: ${arguments} smart contract address is: ${contractAddress}`);

    // fire away!
    sendSigned(txData, function (result) {
        callback(result);
    })

}



//deploy(console.log);
//callSmartContractMethod('printRequested', ['0xa5B064B11640acBEd623D09Ac160B867d27f03ce'], '0x229359B6C1b106df7cF05F3AA89f9b5cE43c02c9', console.log);
//callSmartContractMethod('printOrderReceived', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce', console.log);
//callSmartContractMethod('printStarted', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce');
//allSmartContractMethod('printFinished', ['0x89e4a99fd1d321086245851603952266a746aa9b71073cfd7f6ed2b43f0af45b'], '0xa5B064B11640acBEd623D09Ac160B867d27f03ce');


function subscribeToBlockchainEvents(contractAddress, callback) {
    log.magenta("👀⛓ Subscribed to contracts events. Smart contract is ["+contractAddress+"]");

    let web3 = new Web3(
        // Replace YOUR-PROJECT-ID with a Project ID from your Infura Dashboard
        new Web3.providers.WebsocketProvider("wss://ropsten.infura.io/ws/v3/82ab725d0f624783b1b5a70bdc7d4fbb")
    );

    const instance = new web3.eth.Contract(JSON.parse(fs.readFileSync(process.env.SMART_CONTRACT_JSON).toString()).abi, contractAddress);

    instance.events.GetSateEvent({
        filter: {b_printRequested: [true, false], b_printOrderReceived: [true, false], b_printStarted: [true, false], b_printFinished: [true, false]}, // Using an array means OR: e.g. 20 or 23
        fromBlock: 5000000
    }, function(error, event){  })
        .on('data', function(event){
            log.magenta(`😈 Incoming feed: event data from Blockchain!`);
            //log(event); // same results as the optional callback above
            callback(event);
        })
        .on('changed', function(event){
            log.magenta(`😈 Incoming feed: event changed from Blockchain!`);
        })
        .on('error', function(error){
            log.red.underline(`🚨 Event error?`);
        });
}


module.exports = {web3, deploy, callSmartContractMethod, subscribeToBlockchainEvents};



/*
************************************************************************************************************************
************************************************************************************************************************
NOTA: il seguente codice consente di compilare al volo lo smart contract, per gli scopi del prototipo è inutile, perchè
si dovrebbero cambiare ogni volta ABI e bytecode sia lato Company che lato Manufacturer, in teoria è possibile passarli
anche attraverso le chiamate e gli ordini di stampa (quando si invia il GCODE) ma non ha molto senso, perchè cambiare
ABI significa cambiare anche le funzioni dello smart contrcat, di coneseguenza significa anche dover modificare i rombi
dell'architettura con blockchain. In conclusione, se si modifica lo smart contract, allora si deve modificare anche il
codice sorgente delle due App (Company e Manufacturer).
************************************************************************************************************************
************************************************************************************************************************


const path = require('path');  //builds a path from the current file.

//Need the solidity compiler
const solc = require('solc');

//__dirname will get the inbox dir path.
//look in the contracts folder and get a path to the Inbox.sol file.
const myContractPath = path.resolve(__dirname,'contracts','myContract.sol');

//Read in the contents of the file. The raw source code.
const source = fs.readFileSync(myContractPath,'utf8');

//log(solc.compile(source,1));
//This will export the compiled file. Make it available
//At the moment only interested in the Inbox contract.
function compile() {
    var compiledContract = solc.compile(source,1).contracts[':MyContract'];
    log(compiledContract);
    return compiledContract;
}
*/

//getContractEvents('0x0E42ceC3b848C4C13b44471A5c51Bc5FFf821871', log);

