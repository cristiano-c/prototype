pragma solidity ^0.5.0;

contract PrintProofOfWork {

    address  company = 0x8882528C7104e146E0500203C353C09922575385; // use this for deploy!
    //address  company = 0xCA35b7d915458EF540aDe6068dFe2F44E8fa733c; // use it only on Remix IDE
    address  printer = 0xddCf0FeC78b5a20B9138F5DDb1357E3B60026a67;

    event GetSateEvent(
        bool b_printRequested,
        bool b_printOrderReceived,
        bool b_printStarted,
        bool b_printFinished,
        bytes32 gcodeHashCompany,
        bytes32 gcodeHashReceived,
        bytes32 gcodeHashPrinter,
        bytes32 printLogHash
    );

    bool b_printRequested = false;        // Step 1
    bool b_printOrderReceived = false;    // Step 2
    bool b_printStarted = false;          // Step 3
    bool b_printFinished = false;         // Step 4

    bytes32 gcodeHashCompany; // Hash of the gcode used by Company
    bytes32 gcodeHashReceived; // Hash of the gcode received by Manufacturer
    bytes32 gcodeHashPrinter; // Hash of the gcode used by printer
    bytes32 printLogHash;

    constructor() public payable {
        require(msg.sender == company);
    }

    // Step 1
    function printRequested(bytes32 _gcodeHash) public {
        require(msg.sender == company);
        gcodeHashCompany = _gcodeHash;
        b_printRequested = true;
        getCurrentState();
    }

    // Step 2
    function printOrderReceived(bytes32 _gcodeHash) public {
        require(msg.sender == printer);
        gcodeHashReceived = _gcodeHash;
        b_printOrderReceived = true;
        getCurrentState();
    }

    // Step 3
    function printStarted(bytes32 _gcodeHash) public {
        require(msg.sender == printer);
        gcodeHashPrinter = _gcodeHash;
        b_printStarted = true;
        getCurrentState();
    }

    // Step 4
    function printFinished(bytes32 _gcodeHash) public {
        require(msg.sender == printer);
        printLogHash = _gcodeHash;
        b_printFinished = true;
        getCurrentState();
    }

    function getCompanyHash() public view returns (bytes32) {
        return gcodeHashCompany;
    }

    function getPrinterHash() public view returns (bytes32) {
        return gcodeHashPrinter;
    }

    function getCurrentState() public returns (string memory) {
        emit GetSateEvent(b_printRequested, b_printOrderReceived, b_printStarted, b_printFinished, gcodeHashCompany, gcodeHashReceived, gcodeHashPrinter, printLogHash);
    }
}